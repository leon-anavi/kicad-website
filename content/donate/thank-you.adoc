+++
title = "Thank you for supporting KiCad!"
date = "2021-03-26"
aliases = [ "/thank-you/" ]
headless = true
summary = "Thank you for your donation"
+++

KiCad is user-driven and user-supported.  Your donation helps us to ensure that KiCad
development continues to improve.

{{< aboutlink "/img/donations/2021_flyer.png" "/img/donations/2021_flyer.pdf" >}}